#!/bin/bash

# Erik Tamarit Lechado

# Task1
sudo groupadd -f strings
sudo groupadd -f woodwind
sudo groupadd -f metalwind
sudo groupadd -f percussion
sudo groupadd -f conductor
sudo groupadd -f orchestra


sudo useradd -m -g orchestra -G woodwind -s /bin/bash piccolo
echo "piccolo:1234" | chpasswd
sudo useradd -m -g orchestra -G woodwind -s /bin/bash clarinet
echo "clarinet:1234" | chpasswd
sudo useradd -m -g orchestra -G metalwind -s /bin/bash horn
echo "horn:1234" | chpasswd
sudo useradd -m -g orchestra -G metalwind -s /bin/bash trunk
echo "trunk:1234" | chpasswd
sudo useradd -m -g orchestra -G strings -s /bin/bash fiddle
echo "fiddle:1234" | chpasswd
sudo useradd -m -g orchestra -G strings -s /bin/bash viola
echo "viola:1234" | chpasswd
sudo useradd -m -g orchestra -G strings -s /bin/bash cello
echo "cello:1234" | chpasswd
sudo useradd -m -g orchestra -G strings -s /bin/bash doublebass
echo "doublebass:1234" | chpasswd
sudo useradd -m -g orchestra -G percussion -s /bin/bash battery
echo "battery:1234" | chpasswd
sudo useradd -m -g orchestra -G percussion -s /bin/bash xylophone
echo "xylophone:1234" | chpasswd
sudo useradd -m -g orchestra -G conductor -s /bin/bash conductor
echo "conductor:1234" | chpasswd



# ACT 1

sudo mkdir -p /srv/sox/TheGreatGateOfKiev
sudo mkdir -p /srv/sox/BlueDanube
sudo mkdir -p /srv/sox/NewWorldSymphonie
sudo mkdir -p /srv/sox/TheJazzSuite

ls /home | grep -v $USER | grep -v "Workspace" | grep -v "spectator" > homes.txt

cat homes.txt | while read line; do

echo $line > /srv/sox/TheGreatGateOfKiev/$line.txt | chmod 640 /srv/sox/TheGreatGateOfKiev/$line.txt | chown $line /srv/sox/TheGreatGateOfKiev/$line.txt
echo $line > /srv/sox/BlueDanube/$line.txt | chmod 640 /srv/sox/BlueDanube/$line.txt | chown $line /srv/sox/BlueDanube/$line.txt
echo $line > /srv/sox/NewWorldSymphonie/$line.txt | chmod 640 /srv/sox/NewWorldSymphonie/$line.txt | chown $line /srv/sox/NewWorldSymphonie/$line.txt
echo $line > /srv/sox/TheJazzSuite/$line.txt | chmod 640 /srv/sox/TheJazzSuite/$line.txt | chown $line /srv/sox/TheJazzSuite/$line.txt

done

cd /srv/sox/TheGreatGateOfKiev
chown :strings fiddle.txt viola.txt cello.txt doublebass.txt
chown :woodwind piccolo.txt clarinet.txt
chown :metalwind horn.txt trunk.txt
chown :percussion battery.txt xylophone.txt
chown :conductor conductor.txt

cd /srv/sox/BlueDanube
chown :strings fiddle.txt viola.txt cello.txt doublebass.txt
chown :woodwind piccolo.txt clarinet.txt
chown :metalwind horn.txt trunk.txt
chown :percussion battery.txt xylophone.txt
chown conductor:conductor conductor.txt

cd /srv/sox/NewWorldSymphonie
chown :strings fiddle.txt viola.txt cello.txt doublebass.txt
chown :woodwind piccolo.txt clarinet.txt
chown :metalwind horn.txt trunk.txt
chown :percussion battery.txt xylophone.txt
chown :conductor conductor.txt

cd /srv/sox/TheJazzSuite
chown :strings fiddle.txt viola.txt cello.txt doublebass.txt
chown :woodwind piccolo.txt clarinet.txt
chown :metalwind horn.txt trunk.txt
chown :percussion battery.txt xylophone.txt
chown :conductor conductor.txt

setfacl -R -m u:conductor:rwx /srv/sox

sudo useradd -m -g orchestra -G woodwind -s /bin/bash spectator
echo "spectator:1234" | chpasswd

exit 0
