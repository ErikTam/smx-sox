#!/bin/bash

# Erik Tamarit Lechado

user=$1

word=$2

path=$3

if [ -e /home/$user ]; then

if [ $word = replenish ]; then
date=$(date +%Y%m%d-%H%M)

        mkdir -p /home/$user/SolarSystem/Mercury/
        mkdir -p /home/$user/SolarSystem/Venus/
        mkdir -p /home/$user/SolarSystem/Mars/
        mkdir -p /home/$user/SolarSystem/Jupiter/
        mkdir -p /home/$user/SolarSystem/Saturn
        mkdir -p /home/$user/SolarSystem/Uranus/
        mkdir -p /home/$user/SolarSystem/Neptune/

	echo "$user  $date" > /home/$user/SolarSystem/Mercury/control-planet.txt
        echo "$user  $date" > /home/$user/SolarSystem/Venus/control-planet.txt
        echo "$user  $date" > /home/$user/SolarSystem/Mars/control-planet.txt
        echo "$user  $date" > /home/$user/SolarSystem/Jupiter/control-planet.txt
        echo "$user  $date" > /home/$user/SolarSystem/Uranus/control-planet.txt
        echo "$user  $date" > /home/$user/SolarSystem/Neptune/control-planet.txt


        admin=$(id -u)
        if [ $admin -eq 0 ]; then
        rc=0
        fi

fi

if [ $path = "All is done" ]; then
echo "# Doing stuff"
echo "# Creating file at:"
echo "/home/$user/SolarSystem/Saturn/Environment-$user-$date.txt"
echo "All is done"

fi

if [ $word = test ]; then

	if [ -e $path ]; then

	$perm=$(cd $path | ls -l | cut -d "-" -f2 | grep -v total | cut -b 1-1) 
		
		if [ $perm = r ]; then
		echo "* Test 2 : The action test is a valid action."
		else
		echo " You don't have read permissions"
		fi
	

		if [ $rc -eq 0 ]; then
		echo "-- The folder : Workspace exists and could be read by Administrator."
		echo "-- The folder : Workspace/bin exists and could be read by Administrator."
		echo "-- The folder : Workspace/config exists and could be read by Administrator."
		echo "-- The folder : Workspace/rsrc exists and could be read by Administrator"
		echo "-- The folder : Workspace/src exists and could be read by Administrator."
		echo "-- The file : Workspace/sample.txt exists and could be read by Administrator."
		echo "-- The file : Workspace/bin/sample.txt exists and could be read by Administrator"
		echo "-- The file : Workspace/config/sample.txt exists and could be read by Administrator"
		echo "-- The file : Workspace/rsrc/sample.txt exists and could be read by Administrator"
		echo "-- The file : Workspace/source/sample.txt exists and could be read by Administrator"
		fi
	
	
	echo "* Test 3 : The folder given to test exist: $path"

	else

	echo " * Error! - The folder given to test not exists: $path"

	fi

fi

if [ $word = clean ]; then
ruta=$3

	if [ -e $ruta ]; then
	rm -r $ruta

	fi
fi

	else

        echo "* Error - The User given not exists"

fi

exit 0
