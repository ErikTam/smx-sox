#!/bin/bash

# Erik Tamarit Lechado

if [ $# -eq 0 ]; then

echo "You need to put one argument"

exit 1

fi


if [ $1 = "humano" ]; then
rm /boot/grub/grub.cfg

cp grub-humano.cfg /boot/grub/

fi


if [ $1 = "lobo" ]; then
rm /boot/grub/grub.cfg

cp grub-lobo.cfg /boot/grub/ 

fi 
 
exit 0
