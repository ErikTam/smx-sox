#!/bin/bash

# Erik Tamarit Lechado

sudo mkdir -p /srv/sox/Sinfonietta
sudo mkdir -p /srv/sox/Saturn
sudo mkdir -p /srv/sox/Unfinished
sudo mkdir -p /srv/sox/Valkyres

sudo groupadd -f strings
sudo groupadd -f woodwind
sudo groupadd -f metalwind
sudo groupadd -f percussion
sudo groupadd -f conductor
sudo groupadd -f orchestra


sudo useradd -m -g orchestra -G woodwind -s /bin/bash piccolo
echo "piccolo:1234" | chpasswd
sudo useradd -m -g orchestra -G woodwind -s /bin/bash clarinet
echo "clarinet:1234" | chpasswd
sudo useradd -m -g orchestra -G metalwind -s /bin/bash horn
echo "horn:1234" | chpasswd
sudo useradd -m -g orchestra -G metalwind -s /bin/bash trunk
echo "trunk:1234" | chpasswd
sudo useradd -m -g orchestra -G strings -s /bin/bash fiddle
echo "fiddle:1234" | chpasswd
sudo useradd -m -g orchestra -G strings -s /bin/bash viola
echo "viola:1234" | chpasswd
sudo useradd -m -g orchestra -G strings -s /bin/bash cello
echo "cello:1234" | chpasswd
sudo useradd -m -g orchestra -G strings -s /bin/bash doublebass
echo "doublebass:1234" | chpasswd
sudo useradd -m -g orchestra -G percussion -s /bin/bash battery
echo "battery:1234" | chpasswd
sudo useradd -m -g orchestra -G percussion -s /bin/bash xylophone
echo "xylophone:1234" | chpasswd
sudo useradd -m -g orchestra -G conductor -s /bin/bash conductor
echo "conductor:1234" | chpasswd

ls /home | grep -v $USER | grep -v "Workspace" | grep -v "spectator" > homes.txt

cat homes.txt | while read line; do

echo $line > /srv/sox/Sinfonietta/$line.txt ; chmod 640 /srv/sox/Sinfonietta/$line.txt ; chown $line /srv/sox/Sinfonietta/$line.txt
echo $line > /srv/sox/Saturn/$line.txt ; chmod 640 /srv/sox/Saturn/$line.txt ; chown $line /srv/sox/Saturn/$line.txt
echo $line > /srv/sox/Unfinished/$line.txt ; chmod 640 /srv/sox/Unfinished/$line.txt ; chown $line /srv/sox/Unfinished/$line.txt
echo "The swift Indian bat happily ate cardillo and kiwi, while the stork played the saxophone behind the straw hut .... 0123456789" > /srv/sox/Valkyres/$line.txt ; chmod 640 /srv/sox/Valkyres/$line.txt ; chown $line /srv/sox/Valkyres/$line.txt

done

cd /srv/sox/Sinfonietta
chown :strings fiddle.txt viola.txt cello.txt doublebass.txt
chown :woodwind piccolo.txt clarinet.txt
chown :metalwind horn.txt trunk.txt
chown :percussion battery.txt xylophone.txt
chown :conductor conductor.txt

cd /srv/sox/Saturn
chown :strings fiddle.txt viola.txt cello.txt doublebass.txt
chown :woodwind piccolo.txt clarinet.txt
chown :metalwind horn.txt trunk.txt
chown :percussion battery.txt xylophone.txt
chown conductor:conductor conductor.txt

cd /srv/sox/Unfinished
chown :strings fiddle.txt viola.txt cello.txt doublebass.txt
chown :woodwind piccolo.txt clarinet.txt
chown :metalwind horn.txt trunk.txt
chown :percussion battery.txt xylophone.txt
chown :conductor conductor.txt

cd /srv/sox/Valkyres
chown :strings fiddle.txt viola.txt cello.txt doublebass.txt
chown :woodwind piccolo.txt clarinet.txt
chown :metalwind horn.txt trunk.txt
chown :percussion battery.txt xylophone.txt
chown :conductor conductor.txt

setfacl -R -m u:conductor:rwx /srv/sox

ls -l /srv/sox/Saturn | grep wind | awk '{print $9}' | while read sat; do

chmod 660 /srv/sox/Saturn/$sat

done

v=$(ls /srv/sox/Unfinished | grep v | grep "v*")

chmod 641 /srv/sox/Unfinished/$v

setfacl -R -m u:conductor:rwx /srv/sox/Valkyres

touch /srv/sox/Sinfonietta/TheSilencio.txt
chown * /srv/sox/Sinfonietta/TheSilencio.txt
exit 0
