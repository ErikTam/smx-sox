#!/bin/bash

# Erik Tamarit Lechado

atc=$1
def=$2

cont=1

hori=$(cat tabla.txt | sed -n 2,20p  | grep $atc)

for i in Normal Fire Water Electric Grass Ice Fighting Poison Ground Flying Psychic Bug Rock Ghost Dragon Dark Steel Fairy; do

((cont++))

if [ $i = $2 ]; then

echo $hori | cut -d "," -f$cont

fi


done


exit 0
