#!/bin/bash

# Erik Tamarit Lechado

while true; do

#sleep 5


# Root test
root=$(id -u)

if [ $root = 0 ]; then

echo "* You Have Root Permissions"

else

echo "* You aren't root"

fi


# date
date | cut -d " " -f5 | cut -d ":" -f1,2


# Mount check
if [ -e /mnt/finished ]; then

echo "* The mount point exists at /mnt/finished/"

else

mkdir /mnt/finished

fi


# Mount
mount tic.ieslasenia.org:/srv/nfs/finished-on-server /mnt/finished


# List folders
echo "* The cards present are:"
ls /mnt/finished | sort


#Count Gaps
gap=$(ls /mnt/finished | sort | cut -d "-" -f2)

((gaps++))


if ! [ $gap = $gaps ]; then
((rc++))

fi

echo $rc

done

exit 0
