#!/bin/bash


mkdir /backup

backup="/backup"
# Fecha
date=$(date +%F)

# Ficheros de configuración a copiar
CONFIG_FILES=$(
"/etc/ldap/ldap.conf"
"/etc/freeradius/3.0/radiusd.conf"
"/etc/freeradius/3.0/sites-enable/my_server"
"/etc/freeradius/3.0/mods-enable/ldap"
"/etc/freeradius/3.0/clients.conf"
"/etc/dnsmasq.conf"
"/etc/dnsmasq.d/wifi.conf"
"/etc/sysconfig/iptables"
"/etc/hosts"
"/etc/NetworkManager/NetworkManager.conf"
"/etc/apache2/apache2.conf"
"/etc/ssh/sshd_config"
"/etc/netplan/00-installer-config.yaml"
"/etc/hostapd/hostapd.conf"
)

# Copia los ficheros de configuración al directorio de backup
for file in $CONFIG_FILES; do
	mkdir $backup/$date
	cp $file $backup/$date/
done

exit 0
