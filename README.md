# Repositorios de SOX

# Índice
- [Samba](#samba)
- [LDAP](#ldap)
- [Freeradius](#freeradius)

Aqui se almacenan los recursos y tutoriales utilizados en SOX(Sistemas operativos en red)

## Samba <a name="samba"></a>

### Montar samba en windows server y compartir a un linux
1. Tenemos que instalar dos máquinas virtuales una Windows y una Linux, tienen que estar en la misma red y deben tener ip’s distintas, o en adaptador puente

2. Le damos a añadir roles y seleccionamos el los dos primeros que ponga “ Directory”, le damos a reiniciar cuando este listo y instalamos.

3. Luego añadimos un nuevo recurso compartido y seleccionamos el archivo que queramos, para seleccionar “Habilitar enumeración basada en el acceso” y creamos.

![Paso 3](/Capturas/wind.png)

4. Ahora en linux creamos un “.credentials” y poner:
username=usuario
password=contraseña
domain=WORKGROUP

5. Le damos permisos: 
``` {.example} 
chmod 0600 .credentials
```


6. Lo siguiente lo puedes hacer con el fstab o con el siguiente comando pero tendras que instalar el cifs-tools: 
```{.example}
Mount.cifs "//192.168.4.10/Compartido/" /client/compartido-in-SMB/ -o "user=user,password=password"
```
fstab Ejemplo:
![Paso 6](/Capturas/fstab.png)


# PHPLDAPADMIN <a name="ldap"></a>

## Paso 1

Primero descargamos el phpldapadmin de aqui:
```{.example}
wget https://gitlab.com/ErikTam/smx-sox/-/blob/main/phpLDAPadmin-1.2.6.4_1_.tar.gz
```
Y cambiamos el /usr/share/phpldapadmin por el que descomprimimos.
Y luego descargamos "slapd" y "mysql-server"

Luego configuramos slapd:
```{.example}
sudo dpkg-reconfigure slapd
```

![Paso 7](/Capturas/slapd-1.png)

![Paso 8](/Capturas/slapd-2.png)

Esto equivaldría a un DN base de: <dc=ubuntusrvXX,dc=smx2023,dc=net>.

![Paso 9](/Capturas/slapd-3.png)

![Paso 10](/Capturas/slapd-4.png)

En esta pregunta nos indica qué debe hacer el gestor de software si decidimos borrar el paquete, lo que marcaremos será NO Borrar la base de datos en caso de desinstalación del servicio slapd para posibles recuperaciones frente a desastres.

Esto nos marca también que si queremos borrar la base de datos tendremos que realizar esta operación manualmente, y no nos servirá el proceso de desinstalación del servidor de LDAP.

Por último, nos pregunta si deseamos mover la base de datos antigua (que está vacia en este momento) durante este proceso. Optaremos por la opción: Sí

Para solucionar el
```{.example}
sudo nano /etc/php/8.1/apache2/php.ini
```
y ponemos memory_limit= 512M y reiniciamos apache2

Para ver la pagina buscamos en google: http://HERE-MY-IP/phpldapadmin/

Luego vamos a: `/etc/phpldapadmin/config.php`

![Paso 11](/Capturas/slapd-5.png)

Y ahora nos hacemos login con "cn=admin,dc=ubuntusrv,dc=smx2023,dc=net"

Ejemplo:
![Paso 12](/Capturas/slapd-6.png)


Crearemos primero la el Grupo: "Posix group"
Luego le damos click al grupo y creamos un "User Account"
Y luego el otro grupo.

La estructura quedará así:

![Paso 13](/Capturas/sladp-7.png)



Para poder acceder a los datos contenidos en el LDAP desde la terminal, podemos instalar una serie de utilidades nos permitirán realizar consultas y modificaciones.

AVISO:
Las `ldap-utils` pueden ser instaladas en cualquier máquina para realizar consultas.
`sudo apt install ldap-utils`
Una vez realizada la instalación, podemos comprobar que somos capaces de contactar con el `ldap server` utilizando la línea de comandos:
```{.example}
ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://IP-SERVER
```
Debé funcionar con : https://ldap.smx2023.net o no.


## Paso 2

En el cliente hacemos esto:

```{.example}
sudo apt install libnss-ldap libpam-ldap ldap-utils
```

ldapi                 `ldapi:\\ubuntusrv.smx2023.net`
distinguished-name    `dc=ubuntusrv,dc=smx2023,dc=net`
ldap-version          `3`
Root Database Admin   `Yes`
LDAP Database Login   `No`
LDAP Account Root     `cn=admin,dc=ubuntusrv,dc=smx2023,dc=net`
LDAP Pass             `Lin4dm1n`

![Paso 14](/Capturas/paso2-1.png)
![Paso 15](/Capturas/paso2-2.png)
![Paso 16](/Capturas/paso2-3.png)

Luego en el server hacemos esto:
Instalamos los paquetes gnutls-bin y ssl-cert:
```{.example}
sudo apt install gnutls-bin ssl-cert
```
Crear una llave privada para la Autoridad del Certificado:
```{.example}
sudo certtool --generate-privkey --bits 4096 --outfile /etc/ssl/private/mycakey.pem
```
Crea el template/file `/etc/ssl/ca.info` para definir el CA:
```{.example}
cn = smx2023 (compañia)
ca
cert_signing_key
expiration_days = 3650
```
Crear el auto-asignado en el certificado CA :
```{.example}
sudo certtool --generate-self-signed \
--load-privkey /etc/ssl/private/mycakey.pem \
--template /etc/ssl/ca.info \
--outfile /usr/local/share/ca-certificates/mycacert.crt
```

Y ahora esto:
```{.example}
sudo update-ca-certificates
Updating certificates in /etc/ssl/certs...
1 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.
```
Esto tambien crea: /etc/ssl/certs/mycacert.pem con un enlace simbolico con /usr/local/share/ca-certificates.

Crea una llave privada para el server
```{.example}
sudo certtool --generate-privkey \
--bits 2048 \
--outfile /etc/ldap/ldap01_slapd_key.pem
```

Crea el archivo `/etc/ssl/ldap01.info` y metele esto:
```{.example}
organization = Example Company
cn = ubuntusrv.smx2023.net
tls_www_server
encryption_key
signing_key
expiration_days = 365
```
El certificado solo dura por 1 año yu solo es valido para ubuntusrv.smx2023.net.

Crea el certificado del server:
```{.example}
sudo certtool --generate-certificate \
--load-privkey /etc/ldap/ldap01_slapd_key.pem \
--load-ca-certificate /etc/ssl/certs/mycacert.pem \
--load-ca-privkey /etc/ssl/private/mycakey.pem \
--template /etc/ssl/ldap01.info \
--outfile /etc/ldap/ldap01_slapd_cert.pem
```
Ajusta los permisos:
```{.example}
sudo chgrp openldap /etc/ldap/ldap01_slapd_key.pem
sudo chmod 0640 /etc/ldap/ldap01_slapd_key.pem
```
Ahora ya se puede configurar.

Crea el fichero `certinfo.ldif` con lo siguiente.
```{.example}
dn: cn=config
add: olcTLSCACertificateFile
olcTLSCACertificateFile: /etc/ssl/certs/mycacert.pem
-
add: olcTLSCertificateFile
olcTLSCertificateFile: /etc/ldap/ldap01_slapd_cert.pem
-
add: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/ldap/ldap01_slapd_key.pem
```
Use el comando ldapmodify para informarle a slapd sobre nuestro trabajo TLS a través de la base de datos slapd-config:
```{.example}
sudo ldapmodify -Y EXTERNAL -H ldapi:/// -f certinfo.ldif
```
Si necesita acceso a LDAPS (LDAP sobre SSL), debe editar `/etc/default/slapd` e incluir ldaps:/// en SLAPD_SERVICES como se muestra a continuación:

`SLAPD_SERVICES="ldap:/// ldapi:/// ldaps:///"`

Y reinicia el slapd: `sudo systemctl restart slapd.`

Tenga en cuenta que StartTLS estará disponible sin el cambio anterior y NO necesita un reinicio slapd.

Test StartTLS:
```{.example}
ldapwhoami -x -H ldap://ldap01.example.com
anonymous
```

Ahora en el cliente instalamos el SSSD: `sudo apt install sssd libpam-sss libnss-sss`

Luego creamos esto: `sudo nano /etc/sssd/sssd.conf`
Y lo rellenamos con esto:
```{.example}
[sssd]
services = nss, pam, ifp
config_file_version = 2
domains = smx2023.net

[nss]
filter_groups = root
filter_users = root
reconnection_retries = 3

[domain/smx2023.net]
ldap_id_use_start_tls = True
cache_credentials = True
ldap_search_base = dc=ubuntusrv, dc=smx2023,dc=net
id_provider = ldap
debug_level = 3
auth_provider = ldap
chpass_provider = ldap
access_provider = ldap
ldap_schema = rfc2307
ldap_uri = ldap://ubuntusrv.smx2023.net
ldap_default_bind_dn = cn=admin,dc=ubuntusrv,dc=smx2023,dc=net
ldap_id_use_start_tls = true
ldap_default_authtok = Lin4dm1n
ldap_tls_reqcert = demand
ldap_tls_cacert = /etc/ssl/certs/ldapcacert.crt
ldap_tls_cacertdir = /etc/ssl/certs
ldap_search_timeout = 50
ldap_network_timeout = 60
ldap_access_order = filter
ldap_access_filter = (objectClass=posixAccount)
ldap_user_search_base = cn=goblins,dc=ubuntusrv,dc=smx2023,dc=net
ldap_user_object_class = inetOrgPerson
ldap_user_gecos = cn
enumerate = True
debug_level = 0x3ff0
```


Si el server esta escuchando en STARTTLS en el puerto 389 (via tcp o udp), usa este comando:
```{.example}
openssl s_client -connect ubuntusrv.smx2023.net:389 -starttls ldap -showcerts < /dev/null openssl x509 -text | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'
```
Debe copiar el fragmento del Certificado `(desde BEGIN ASTA END)` y almacenarlo en la RUTA que indicó en su propio sssd.conf
Validar el Certificado:
```{.example}
openssl s_client -connect ubuntusrv.smx2023.net:389 -CAfile /etc/ssl/certs/ldapcacert.crt
```
El resultado debe ser algo similar a:

`Verification: OK`
`Verify return code: 0 (ok)`

Ahora, en el archivo /etc/ldap/ldap.conf, establezca TLS_CACERT en la RUTA del certificado de CA que acabamos de crear.

Permisos en SSSD
Los permisos de /etc/sssd/ (archivos y subcarpetas) deben ser:

Owner : root:root
Permissions: 0600

Y reiniciar el sssd
```{.example}
systemctl restart sssd
```

Ahora instalamos el IFP
```{.example}
sudo apt install libsss-simpleifp0
```

### pam-mkhomedir
Ahora vamos a habilitar el módulo PAM que genera automáticamente el
HOME Directorio de usuarios al iniciar sesión (si no existe).
Edite el archivo:
/etc/pam.d/common-session

Y debajo de la línea:

`session optional pam_sss.so`

Escribe:
```{.example}
session required        pam_mkhomedir.so skel=/etc/skel/ umask=0022
```

### Comprobaciones
Pruebe si el usuario goblin01 en la salida del comando:
`getent passwd goblin01`

# Freeradius <a name="freeradius"></a>
Cambiar el `/etc/hosts` y poner el nombre de la pagina.
```{.example}
sudo apt update | sudo apt upgrade
```
```{.example}
sudo apt install freeradius freeradius-ldap apache2 && sudo systemctl enable --now apache2
```
```{.example}
nano /etc/freeradius/3.0/sites-enabled/my_server
```
```{.example}
server my_server {
listen {
        type = auth
        ipaddr = *
        port = 1812
}
authorize {
        ldap
        if (ok || updated)  {
        update control {
        Auth-Type := ldap
        }
        }
}
authenticate {
        Auth-Type LDAP {
                ldap
        }
}
}
```
```{.example}
nano /etc/freeradius/3.0/mods-enabled/ldap
```
```{.example}
ldap {
    server = 'ubuntusrv.smx2023.net'
    identity = 'cn=admin,dc=ubuntusrv,dc=smx2023,dc=net'
    password = 1
    base_dn = 'dc=ubuntusrv,dc=smx2023,dc=net'
    user {
        base_dn = "${..base_dn}"
        filter = "(cn=%{%{Stripped-User-Name}:-%{User-Name}})"
        scope = 'sub'
    }
    group {
        base_dn = 'cn=goblins,dc=ubuntusrv,dc=smx2023,dc=net'
        filter = '(objectClass=groupOfUniqueNames)'
        membership_attribute = "memberOf"
        scope = 'sub'
    }
}
```
```{.example}
nano /etc/freeradius/3.0/clients.conf
```
```{.example}
client cisco_router {
    ipaddr = 192.168.5.123
    secret = testing123
    require_message_authenticator = yes
}
```
```{.example}
rm /etc/freeradius/mods-enable/eap
```

```{.example}
freeradius -X
```

```{.example}
radtest goblin01 password 127.0.0.1 1812 testing123
```
## DNSMASQ con Freeradius-LDAP
Ahora instalamos y configuramos el dnsmasq
```{.example}
sudo apt install dnsmasq
```
Y ahora detenmos el resolved para que nos deje reiniciar.
```{.example}
systemctl stop systemd-resolved.service
```
```{.example}
nano /etc/dnsmasq.conf
```
Borra todo el contenido o busca la linea de comando.
```{.example}
conf-dir=/etc/dnsmasq.d/,*conf
```
Ahora crea un .conf, se puede llamar de cualquier manera.
```{.example}
nano /etc/dnsmasq.d/wifi.conf
```
Y añadimos esto=
```{.example}
interface=wlakfha

dhcp-range=10.0.2.10,10.0.2.254,12h

listen-address=::1,127.0.0.1

server=8.8.8.8

cache-size=1000
```
En `interface` ponemos la interfaz del punto de acceso, en `dhcp-range` el rango de IPs que ofrecerá y durante cuanto tiempo, en `listen-address`ponemos la ip del localhost (recomendable), en `server` los servidores dns que redireccionara el punto de acceso.



