#!/bin/bash

# Erik Tamarit Lechado


if [ -e $2 ]; then
echo "* Working on $1"
else
echo "Only one parameter"
fi

if [ $1 = "clean" ]; then

	if [ -e $HOME/games ]; then
	rm $HOME/games
	else
	echo "Is already clean"
	fi
fi

if [ $1 = "start" ]; then
rc=0
	if [ -e $HOME/Friday-"for"-Friday ]; then
	echo "Already exist"

	else

	mkdir $HOME/Friday-for-Friday
	date | cut -d " " -f1,2,3,4,5 > $HOME/Friday-for-Friday/time-friday-start.tkn

	fi

fi

if [ $1 = "green" ]; then
mkdir $HOME/Friday-for-Friday/Green-Level
date | cut -d " " -f1,2,3,4,5 > $HOME/Friday-for-Friday/Green-Level/time-friday-green.tkn

rc=1

fi

if [ -e $HOME/Friday-for-Friday/Green-Level ]; then

	if [ $1 = "yellow" ]; then
	mkdir $HOME/Friday-for-Friday/Yellow-Level
	date | cut -d " " -f1,2,3,4,5 > $HOME/Friday-for-Friday/Yellow-Level/time-friday-yellow.tkn
	fi
else
if [ $rc -eq 1 ]; then 
echo "You have to finish the green level"
fi
fi

if [ -e $HOME/Friday-for-Friday/Yellow-Level ]; then
rc=2
	if [ $1 = "red" ]; then
	mkdir $HOME/Friday-for-Friday/Red-Level
	date | cut -d " " -f1,2,3,4,5 > $HOME/Friday-for-Friday/Red-Level/time-friday-red.tkn
fi
else
if [ $rc -eq 2 ]; then 
echo "You have to finish the yellow level"
fi
fi

if [ -e $HOME/Friday-for-Friday/Red-Level ]; then
rc=3
	if [ $1 = "pirates" ]; then
	mkdir $HOME/Friday-for-Friday/PIRATES
	date | cut -d " " -f1,2,3,4,5 > $HOME/Friday-for-Friday/PIRATES/time-friday-pirates.tkn
fi
else
if [ $rc -eq 3 ]; then
echo "You have to finish the red level"
fi
fi

if [ -e $HOME/Friday-for-Friday/PIRATES ]; then
rc=4
	if [ $1 = "end" ]; then
	mkdir $HOME/Friday-for-Friday/End
	date | cut -d " " -f1,2,3,4,5 > $HOME/Friday-for-Friday/End/time-friday-end.tkn
fi
else
if [ $rc -eq 4 ]; then 
echo "You have to defeat the pirates"
fi
fi
exit 0
