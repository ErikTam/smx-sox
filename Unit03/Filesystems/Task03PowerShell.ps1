﻿# Erik Tamarit Lechado

$abs = $args[0]
 
 $exist = Test-Path $abs

 if ( $exist -eq "True" ){
    Write-Host "Exist"

    if ( $abs.Startswith("C:") -eq "True" ){
    
        Write-Host "Is absolute"

        Write-Host "[Folder Permissions]"

        Get-Acl -Path $abs | Format-Table -Wrap

        Write-Host "[Users that can have acces to the folder]"

        Get-Acl $abs

        }else{

        Write-Host "Is relative"

        Write-Host "[Folder Permissions]"

        Get-Acl -Path $abs | Format-Table -Wrap

        Write-Host "[Users that can have acces to the folder]"

        Get-Acl $abs
    }

    }else{
    Write-Host "Don't exist"
    }
    