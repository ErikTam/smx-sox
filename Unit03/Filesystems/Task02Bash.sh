#!/bin/bash

# Erik Tamarit Lechado

rc=0

path=$1

echo $path | grep -q ^"/" || rc=1


if [ $rc -eq 0 ]; then
        echo "Is absolute"

else

        echo "Is relative"

fi

exit 0
