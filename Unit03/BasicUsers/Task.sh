#!/bin/bash

# Erik Tamarit Lechado

user=$1

word=$2

if [ -e /home/$user ]; then

echo "* Error - The User given not exists"

if [ $word = replenish ]; then
	mkdir /home/Workspace
	mkdir /home/Workspace/config
	mkdir /home/Workspace/bin
	mkdir /home/Workspace/source
	mkdir /home/Workspace/rsrc
	mkdir /home/Workspace/config/sample.txt
	mkdir /home/Workspace/bin/sample.txt
	mkdir /home/Workspace/source/sample.txt
	mkdir /home/Workspace/rsrc/sample.txt
	
	admin=$(id | cut -d "(" -f1 | cut -d "=" -f2)
	if [ $admin -eq 0 ]; then
	rc=0
	fi
fi

if [ $word = test ]; then
path=$3

	if [ -e $path ]; then

	$perm=$(cd $path | ls -l | cut -d "-" -f2 | grep -v total | cut -b 1-1) 
		
		if [ $perm = r ]; then
		echo "* Test 2 : The action test is a valid action."
		else
		echo " You don't have read permissions"
		fi
	

		if [ $rc -eq 0 ]; then
		echo "-- The folder : Workspace exists and could be read by Administrator."
		echo "-- The folder : Workspace/bin exists and could be read by Administrator."
		echo "-- The folder : Workspace/config exists and could be read by Administrator."
		echo "-- The folder : Workspace/rsrc exists and could be read by Administrator"
		echo "-- The folder : Workspace/src exists and could be read by Administrator."
		echo "-- The file : Workspace/sample.txt exists and could be read by Administrator."
		echo "-- The file : Workspace/bin/sample.txt exists and could be read by Administrator"
		echo "-- The file : Workspace/config/sample.txt exists and could be read by Administrator"
		echo "-- The file : Workspace/rsrc/sample.txt exists and could be read by Administrator"
		echo "-- The file : Workspace/source/sample.txt exists and could be read by Administrator"
		fi
	
	
	echo "* Test 3 : The folder given to test exist: $path"

	else

	echo " * Error! - The folder given to test not exists: $path"

	fi

fi

if [ $word = clean ]; then
ruta=$3

	if [ -e $ruta ]; then
	rm $ruta 

	fi

	else

        echo "* Error - The User given not exists"

fi

exit 0
