# Erik Tamarit Lechado
# Me he instalado 3 isos de windows server y en ninguno existian o se podian ver las variables.

if ($args[0] -eq 1){
    Write-Host " * The Chosen variable is windir"
    Write-Host " * You can use to acces: $Env:windir"
    Write-Host " * Store the current Windows Installation Folder."
    Write-Host " * - To find Drivers: $env:windir\System32\Drivers"
    }

if ($args[0] -eq 2){
    Write-Host " * The Chosen variable is Current User"
    Write-Host " * You can use to acces: $Env:USERNAME"
    Write-Host " * Current user’s name."
    Write-Host " * - To do a Write-Host that says your current user"
    }

if ($args[0] -eq 3){
    Write-Host " * The Chosen variable is AppData"
    Write-Host " * You can use to acces: $Env:APPDATA"
    Write-Host " * Point to the location of the Roaming folder in the Application Data folder."
    Write-Host " * - Locating and clearing unnecessary files, settings, and data accumulating in AppData folders"
    }

if ($args[0] -eq 4){
    Write-Host " * The Chosen variable is Home"
    Write-Host " * You can use to acces: $Env:HOMEPATH"
    Write-Host " * Signifies the location of the current user’s profile directory."
    Write-Host " * - To do the same operation on differents users, without changing the script"
    }

if ($args[0] -eq 5){
    Write-Host " * The Chosen variable is Windows Version"
    Write-Host " * You can use to acces: $Env:OS"
    Write-Host " * Is where there are information about the windows version"
    Write-Host " * - To check if a script has to be execute to update the windows version"
    }
    
