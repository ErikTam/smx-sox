#/bin/bash

# Erik Tamarit Lechado

user=$1

grupo=$(ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://192.168.5.158>

if [ $# != 1 ]; then
echo " You have to put 1 argument which is the UserName "
fi

if [ $grupo = "trasgos" ]; then

mount 192.168.5.158:/srv/nfs/exam-ldap-nfs /home/erik/shared/smx2023.net/exam-l>

fi
exit 0
