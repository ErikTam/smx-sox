#/bin/bash

# Erik Tamarit Lechado

user=$1

grupo=$(ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://192.168.5.158 | grep $user | grep "#" | cut -d "," -f2 | cut -d " " -f2)

if [ $# != 1 ]; then
echo " You have to put 1 argument which is the UserName "
fi

if [ $grupo = "goblins" ]; then

mount 192.168.5.158:/srv/troops /home/erik/shared/smx2023.net/troops

fi
exit 0
